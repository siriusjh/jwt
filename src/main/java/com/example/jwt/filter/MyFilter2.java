package com.example.jwt.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

public class MyFilter2 implements Filter {

    private final Logger logger = LoggerFactory.getLogger(MyFilter2.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("필터 2");
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
