package com.example.jwt.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFilter3 implements Filter {
    private  final Logger logger = LoggerFactory.getLogger(MyFilter3.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        logger.info("필터 1");
        // 내가 만든 토큰인지 검증(RSA, HS256)
        if(req.getMethod().equals("POST")) {
            String headerAuth = req.getHeader("Authorization");
            logger.info(headerAuth);
            if(headerAuth.equals("token")) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                PrintWriter out = res.getWriter();
                out.println("인증 안됨");
            }
        }
    }
}
